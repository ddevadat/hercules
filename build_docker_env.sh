#/usr/bin/env sh

PG_DB=mydevopsdb
PG_PASSWORD=`head -c 18 /dev/urandom | base64 | tr -dc 'a-zA-Z0-9' | head -c 12`
SECRET_KEY=`head -c 75 /dev/urandom | base64 | tr -dc 'a-zA-Z0-9' | head -c 50`
PG_SERVICE_NAME=postgres
PG_USER=mydevopsuser
ADMIN_USER=admin
ADMIN_INIT_PASSWORD=admin
ADMIN_EMAIL=admin@example.com
echo "POSTGRES_DB=$PG_DB
POSTGRES_USER=$PG_USER
DATABASE_URL=postgres://$PG_USER:$PG_PASSWORD@$PG_SERVICE_NAME:5432/$PG_DB
ADMIN_USER=$ADMIN_USER
ADMIN_INIT_PASSWORD=$ADMIN_INIT_PASSWORD
ADMIN_EMAIL=$ADMIN_EMAIL" > .django_db_config.env


echo -n "$PG_PASSWORD" > .postgres.env
echo -n "$SECRET_KEY" > .django.env

kubectl create configmap django-db-config --from-env-file=.django_db_config.env -n artemis-app
kubectl create secret generic django-db-secret  --from-file=.postgres.env --from-file=.django.env -n artemis-app

rm .postgres.env .django.env
